
-- Drop table

-- DROP TABLE public.poules;

CREATE TABLE public.poules (
	id serial NOT NULL,
	nom varchar NOT NULL,
	CONSTRAINT poules_pk PRIMARY KEY (id),
	CONSTRAINT poules_un UNIQUE (nom)
);


INSERT INTO public.poules (id,nom) VALUES
(NEXTVAL('poules_id_seq'),'POULE A')
,(NEXTVAL('poules_id_seq'),'POULE B')
,(NEXTVAL('poules_id_seq'),'POULE C')
,(NEXTVAL('poules_id_seq'),'POULE D')
;