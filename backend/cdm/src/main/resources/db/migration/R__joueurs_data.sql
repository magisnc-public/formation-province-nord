DELETE FROM public.joueurs;



INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'FLY_HALF','Dan','Biggar','73','1989-10-16')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'WING','Josh','Adams','14','1995-04-21')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOOSE_FORWARD','Shannon','Frizell','5','1994-02-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOOSE_FORWARD','Luke','Jacobson','2','1997-04-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOOSE_FORWARD','Kieran','Read','122','1985-10-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOOSE_FORWARD','Ardie','Savea','39','1993-10-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOOSE_FORWARD','Matt','Todd','21','1988-03-24')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'SCRUM_HALF','TJ','Perenara','59','1992-01-23')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'SCRUM_HALF','Aaron','Smith','87','1988-11-21')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'SCRUM_HALF','Brad','Weber','2','1991-01-17')
;

INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'FLY_HALF','Beauden','Barrett','78','1991-05-27')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'FLY_HALF','Richie','Mo''unga','12','1994-05-25')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'CENTRE','Ryan','Crotty','45','1988-09-23')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'HOOKER','Rory','Best','120','1982-08-15')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'HOOKER','Sean','Cronin','70','1986-05-06')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'HOOKER','Niall','Scannell','16','1992-04-08')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'PROP','Tadhg','Furlong','36','1992-11-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'PROP','Cian','Healy','91','1987-10-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'PROP','David','Kilcoyne','31','1988-12-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'PROP','Andrew','Porter','18','1996-01-16')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'PROP','John','Ryan','20','1988-08-02')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'LOCK','Tadhg','Beirne','8','1988-01-08')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'LOCK','Iain','Henderson','48','1992-12-21')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'LOCK','Jean','Kleyn','3','1993-08-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'LOCK','James','Ryan','19','1996-07-24')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'BACK_ROW','Jack','Conan','16','1992-07-29')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'BACK_ROW','Jordi','Murphy','29','1991-04-22')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'BACK_ROW','Peter','O''Mahony','59','1989-09-17')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'BACK_ROW','Rhys','Ruddock','23','1990-11-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'BACK_ROW','CJ','Stander','33','1990-04-05')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'BACK_ROW','Josh','van der Flier','19','1993-04-25')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'SCRUM_HALF','Luke','McGrath','14','1993-02-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'SCRUM_HALF','Conor','Murray','77','1989-04-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'CENTRE','Jack','Goodhue','9','1985-06-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'CENTRE','Anton','Lienert-Brown','38','1995-04-15')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'CENTRE','Sonny Bill','Williams','53','1985-08-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'WING','George','Bridge','5','1995-04-01')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'WING','Rieko','Ioane','26','1997-03-18')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'WING','Sevu','Reece','3','1997-02-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'FULLBACK','Jordie','Barrett','12','1997-02-17')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'FULLBACK','Ben','Smith','80','1986-06-01')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'HOOKER','Camille','Chat','22','1995-12-18')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'HOOKER','Guilhem','Guirado','70','1986-06-17')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'HOOKER','Peato','Mauvaka','1','1997-01-10')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'HOOKER','Christopher','Tolofua','8','1993-12-31')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'PROP','Cyril','Baille','13','1993-09-15')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'PROP','Demba','Bamba','6','1998-03-17')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'PROP','Cedate','Gomes Sa','9','1993-08-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'PROP','Jefferson','Poirot','29','1992-11-01')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'PROP','Emerick','Setiano','3','1996-07-19')
;



INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'PROP','Rabah','Slimani','53','1989-10-18')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'LOCK','Paul','Gabrillagues','13','1993-06-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'FLY_HALF','Jack','Carty','7','1992-08-31')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'FLY_HALF','Jonathan','Sexton','84','1985-07-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'CENTRE','Bundee','Aki','20','1990-04-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'CENTRE','Chris','Farrell','7','1993-03-16')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'CENTRE','Robbie','Henshaw','38','1993-06-12')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'CENTRE','Garry','Ringrose','24','1995-01-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'WING','Keith','Earls','78','1987-10-02')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'WING','Jordan','Larmour','16','1997-06-01')
;

INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'WING','Jacob','Stockdale','21','1996-04-06')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'FULLBACK','Andrew','Conway','15','1991-07-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'FULLBACK','Rob','Kearney','92','1986-03-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'HOOKER','Shota','Horie','61','1986-01-21')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'HOOKER','Takuya','Kitade','1','1992-09-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'HOOKER','Atsushi','Sakate','17','1993-01-21')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'PROP','Keita','Inagaki','29','1990-06-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'PROP','Koo','Ji-won','8','1194-07-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'PROP','Yusuke','Kizu','3','1995-12-02')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'PROP','Isileli','Nakajima','3','1989-07-09')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOCK','Uwe','Helu','14','1990-07-12')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOCK','James','Moore','3','1993-06-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOCK','Luke','Thompson','67','1981-04-16')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOCK','Wimpie','van der Walt','12','1989-01-06')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOOSE_FORWARD','Kazuki','Himeno','12','1994-07-27')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOOSE_FORWARD','Michael','Leitch','63','1988-10-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOOSE_FORWARD','Lappies','Labuschagné','3','1989-01-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOOSE_FORWARD','Amanaki','Mafi','25','1990-01-21')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOOSE_FORWARD','Yoshitaka','Tokunaga','12','1992-04-10')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'LOOSE_FORWARD','Hendrik','Tui','44','1987-12-13')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'SCRUM_HALF','Kaito','Shigeno','10','1990-11-21')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'SCRUM_HALF','Fumiaki','Tanaka','70','1985-01-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'FLY_HALF','Rikiya','Matsuda','20','1994-05-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'FLY_HALF','Yu','Tamura','57','1989-01-09')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'CENTRE','Timothy','Lafaele','18','1991-08-19')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'CENTRE','Ryoto','Nakamura','19','1991-01-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'CENTRE','Will','Tupou','10','1190-07-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'WING','Kenki','Fukuoka','34','1992-09-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'WING','Lomano','Lemeki','11','1989-01-30')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'WING','Ataata','Moeakiola','4','1996-02-06')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'FULLBACK','Kotaro','Matsushima','34','1993-02-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'FULLBACK','Ryohei','Yamanaka','13','1988-06-22')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'HOOKER','Dane','Coles','64','1986-12-10')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'HOOKER','Liam','Coltman','6','1990-01-25')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'HOOKER','Codie','Taylor','45','1991-03-31')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'PROP','Joe','Moody','41','1988-09-18')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'PROP','Atu','Moli','2','1995-06-12')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'PROP','Angus','Ta''avao','8','1990-03-22')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'PROP','Ofa','Tu''ungafasi','30','1992-04-19')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOCK','Scott','Barrett','31','1993-11-20')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOCK','Brodie','Retallick','77','1991-05-31')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOCK','Patrick','Tuipulotu','25','1993-01-23')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOCK','Sam','WhiteLOCK','112','1988-10-12')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'WING','Hallam','Amos','20','1994-09-24')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'WING','George','North','86','1992-04-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'FULLBACK','Leigh','Halfpenny','82','1988-12-22')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'FULLBACK','Liam','Williams','58','1991-04-09')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'HOOKER','Schalk','Brits','13','1981-05-16')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'HOOKER','Malcolm','Marx','27','1994-07-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'HOOKER','Bongi','Mbonambi','30','1991-01-07')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'PROP','Thomas','du Toit ','10','1995-05-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'PROP','Steven','Kitshoff','40','1992-02-10')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'PROP','Vincent','Koch','15','1990-03-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'SCRUM_HALF','Herschel','Jantjies','4','1996-04-22')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOCK','Lood','de Jager','40','1992-12-17')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOCK','Eben','Etzebeth','79','1991-10-29')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOCK','Franco','Mostert','32','1990-11-27')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOCK','RG','Snyman','16','1995-01-29')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOOSE_FORWARD','Siya','Kolisi','43','1991-06-16')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOOSE_FORWARD','Francois','Louw','69','1985-06-15')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOOSE_FORWARD','Kwagga','Smith','4','1993-06-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOOSE_FORWARD','Duane','Vermeulen','49','1986-07-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'PROP','Frans','Malherbe','32','1991-03-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'PROP','Tendai','Mtawarira','111','1985-08-01')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'PROP','Trevor','Nyakane','41','1989-05-04')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'LOCK','Cory','Hill','24','1992-02-10')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'LOCK','Alun Wyn',' Jones','128','1985-09-19')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'BACK_ROW','James','Davies','5','1990-10-25')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'BACK_ROW','Ross','Moriarty','34','1994-04-18')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'BACK_ROW','Josh','Navidi','19','1990-12-30')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'BACK_ROW','Aaron','Shingler','20','1987-08-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'BACK_ROW','Justin','Tipuric','66','1989-08-06')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'BACK_ROW','Aaron','Wainwright','12','1997-09-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'SCRUM_HALF','Aled','Davies','19','1992-08-19')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'SCRUM_HALF','Gareth','Davies','44','1990-08-18')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'SCRUM_HALF','Tomos','Williams','9','1995-01-01')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'FLY_HALF','Rhys','Patchell','13','1993-05-17')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'CENTRE','Jonathan','Davies','76','1988-04-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'CENTRE','Hadleigh','Parkes','18','1987-10-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'CENTRE','Owen','Watkin','16','1996-10-12')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'LOCK','Rob','Simmons','98','1989-04-19')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'BACK_ROW','Jack','Dempsey','11','1994-04-12')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'BACK_ROW','Michael','Hooper','95','1991-10-29')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'BACK_ROW','Isi','Naisarani','4','1995-02-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'BACK_ROW','David','Pocock','78','1988-04-23')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'BACK_ROW','Lukhan','Salakaia-Loto','16','1996-09-19')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'SCRUM_HALF','Will','Genia','105','1988-01-17')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'SCRUM_HALF','Nic','White','26','1990-06-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'FLY_HALF','Bernard','Foley','70','1989-09-08')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'FLY_HALF','Christian','Lealiifano','22','1987-09-24')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'CENTRE','Samu','Kerevi','29','1993-09-27')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'CENTRE','Tevita','Kuridrani','60','1991-03-31')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'CENTRE','James','O''Connor','48','1990-07-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'WING','Adam','Ashley-Cooper','119','1984-03-27')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'WING','Reece','Hodge','37','1994-08-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'WING','Marika','Koroibete','24','1992-07-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'WING','Jordan','Petaia','0','2000-03-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'FULLBACK','Kurtley','Beale','87','1989-01-06')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'HOOKER','Elliot','Dee','22','1994-03-07')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'HOOKER','Ryan','Elias','8','1995-01-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'HOOKER','Ken','Owens','67','1987-01-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'PROP','Rhys','Carré','1','1998-02-08')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'PROP','Wyn','Jones','15','1992-02-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'PROP','Dillon','Lewis','15','1996-01-04')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'PROP','Nicky','Smith','31','1994-04-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'LOCK','Jake','Ball','36','1991-06-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'LOCK','Adam','Beard','16','1996-01-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'LOCK','Bradley','Davies','64','1987-01-09')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'LOCK','Sébastien','Vahaamahina','42','1991-10-21')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'BACK_ROW','Gregory','Alldritt','7','1997-03-23')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'BACK_ROW','Yacouba','Camara','15','1994-06-02')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'BACK_ROW','Arthur','Iturria','13','1194-05-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'BACK_ROW','Wenceslas','Lauret','24','1988-03-30')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'BACK_ROW','Bernard','Le Roux','33','1989-01-04')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'BACK_ROW','Louis','Picamoles','79','1986-02-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'SCRUM_HALF','Antoine','Dupont','17','1996-11-15')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'SCRUM_HALF','Maxime','Machenaud','36','1988-12-30')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'SCRUM_HALF','Baptiste','Serin','30','1994-06-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'FLY_HALF','Camille','Lopez','24','1989-04-03')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'FLY_HALF','Romain','Ntamack','8','1999-05-01')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'CENTRE','Pierre-Louis','Barassi','0','1998-04-22')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'CENTRE','Gaël','Fickou','48','1194-03-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'CENTRE','Wesley','Fofana','48','1988-01-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'CENTRE','Virimi','Vakatawa','18','1992-05-01')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'WING','Sofiane','Guitoune','7','1989-03-27')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'WING','Yoann','Huget','59','1987-06-02')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'WING','Damian','Penaud','13','1996-09-25')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'WING','Alivereti','Raka','2','1994-12-09')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'FULLBACK','Maxime','Médard','59','1986-11-16')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'FULLBACK','Thomas','Ramos ','7','1995-07-23')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'HOOKER','Folau','Fainga''a','11','1995-05-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'HOOKER','Tolu','Latu','15','1993-02-23')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'HOOKER','Jordan','Uelese','4','1997-01-24')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'PROP','Allan','Alaalatoa','33','1994-01-28')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'PROP','Sekope','Kepu','106','1986-02-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'PROP','Scott','Sio','59','1991-10-16')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'PROP','James','Slipper','91','1989-06-06')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'PROP','Taniela','Tupou','16','1996-05-10')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'LOCK','Rory','Arnold','22','1990-07-04')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'LOCK','Adam','Coleman','34','1991-10-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'LOCK','Izack','Rodda','21','1996-08-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='AUS'),'CENTRE','Matt','To''omua','47','11990-01-02')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='WAL'),'PROP','Tomas','Francis','43','1992-04-27')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'BACK_ROW','Charles','Ollivon','8','1993-05-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='FR'),'WING','Vincent','Rattez','2','1992-03-24')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'LOOSE_FORWARD','Sam','Cane','63','1992-01-13')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='IRE'),'FLY_HALF','Joey','Carbery','19','1995-11-01')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'PROP','Asaeli','Ai Valu','9','1989-05-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'WING','Jonny','May','47','1990-04-01')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'WING','Ruaridh','McConnochie','1','1991-10-23')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'WING','Jack','Nowell','33','1993-04-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'WING','Anthony','Watson','36','1994-02-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'FULLBACK','Elliot','Daly','33','1992-10-08')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'SCRUM_HALF','Willi','Heinz','4','1986-11-24')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'SCRUM_HALF','Ben','Youngs','89','1989-09-05')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'FLY_HALF','Owen','Farrell','73','1991-09-24')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'FLY_HALF','George','Ford','59','1993-03-16')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'CENTRE','Piers','Francis','8','1990-06-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'CENTRE','Jonathan','Joseph','42','1991-05-21')
;

INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'CENTRE','Henry','Slade','22','1993-03-19')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'CENTRE','Manu','Tuilagi','35','1991-05-18')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'WING','Joe','Cokanasiga','8','1997-11-15')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'BACK_ROW','Lewis','Ludlam','2','1995-12-08')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'BACK_ROW','Sam','Underhill','10','1996-07-22')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'BACK_ROW','Mark','Wilson','15','1989-10-06')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'BACK_ROW','Billy','Vunipola','45','1992-11-03')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'LOCK','George','Kruis','35','1990-02-22')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'LOCK','Joe','Launchbury','61','1991-04-12')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'LOCK','Courtney','Lawes','75','1989-02-23')
;

INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'BACK_ROW','Tom','Curry','13','1998-06-15')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'PROP','Joe','Marler','62','1990-07-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'PROP','Kyle','Sinckler','25','1993-03-30')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'PROP','Mako','Vunipola','54','1991-01-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'LOCK','Maro','Itoje','29','1994-10-28')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'HOOKER','Jamie','George','40','1990-10-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'HOOKER','Jack','Singleton','2','1996-05-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'PROP','Dan','Cole','89','1987-05-09')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'PROP','Ellis','Genge','12','1995-02-16')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'WING','S''busiso','Nkosi','8','1996-01-21')
;
INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'FULLBACK','Warrick','Gelant','7','1995-05-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'FULLBACK','Willie','le Roux','56','1989-08-18')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='ENG'),'HOOKER','Luke','Cowan-Dickie','15','1993-06-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'CENTRE','Jesse','Kriel','45','1994-02-15')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'CENTRE','François','Steyn','61','1987-05-14')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'WING','Cheslin','Kolbe','10','1993-10-25')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'WING','Makazole','Mapimpi','8','1990-07-26')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'SCRUM_HALF','Faf','de Klerk','25','1991-10-19')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'SCRUM_HALF','Cobus','Reinach','12','1990-02-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'FLY_HALF','Elton','Jantjies','35','1990-08-01')
;

INSERT INTO public.joueurs (id,id_equipe,poste,prenom,nom,nb_selections,date_naissance) VALUES
(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'FLY_HALF','Handré','Pollard','42','1994-03-11')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'FLY_HALF','Damian','Willemse','5','1998-05-07')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'CENTRE','Lukhanyo','Am','9','1993-11-28')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'CENTRE','Damian','de Allende','40','1991-11-25')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='RSA'),'LOOSE_FORWARD','Pieter-Steph','du Toit','50','1992-08-20')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='JPN'),'SCRUM_HALF','Yutaka','Nagare','19','1992-09-04')
,(NEXTVAL('joueurs_id_seq'),(select id from equipes where code='NZL'),'PROP','Nepo','Laulala','20','1991-11-06')
;