CREATE TABLE public.equipes (
	id serial NOT NULL,
	nom varchar NOT NULL,
	code varchar NOT NULL,
	id_poule int8,
	CONSTRAINT equipes_pk PRIMARY KEY (id),
	CONSTRAINT equipes_un_code UNIQUE (code),
	CONSTRAINT equipes_un_nom UNIQUE (nom),
	CONSTRAINT equipes_fk FOREIGN KEY (id_poule) REFERENCES poules(id)
);

INSERT INTO public.equipes (id,nom,code,id_poule) VALUES
(NEXTVAL('equipes_id_seq'),'Ecosse','SCO',1)
,(NEXTVAL('equipes_id_seq'),'Japon','JPN',1)
,(NEXTVAL('equipes_id_seq'),'Irlande','IRE',1)
,(NEXTVAL('equipes_id_seq'),'Samoa','SAM',1)
,(NEXTVAL('equipes_id_seq'),'Russie','RUS',1)
,(NEXTVAL('equipes_id_seq'),'Nouvelle-Zélande','NZL',2)
,(NEXTVAL('equipes_id_seq'),'Angleterre','ENG',3)
,(NEXTVAL('equipes_id_seq'),'Fidji','FIJ',4)
,(NEXTVAL('equipes_id_seq'),'Australie','AUS',4)
,(NEXTVAL('equipes_id_seq'),'Géorgie','GEO',4)
;
INSERT INTO public.equipes (id,nom,code,id_poule) VALUES
(NEXTVAL('equipes_id_seq'),'Uruguay','URU',4)
,(NEXTVAL('equipes_id_seq'),'Pays de Galles','WAL',4)
,(NEXTVAL('equipes_id_seq'),'Argentine','ARG',3)
,(NEXTVAL('equipes_id_seq'),'France','FR',3)
,(NEXTVAL('equipes_id_seq'),'Tonga','TGA',3)
,(NEXTVAL('equipes_id_seq'),'USA','USA',3)
,(NEXTVAL('equipes_id_seq'),'Namibie','NAM',2)
,(NEXTVAL('equipes_id_seq'),'Afrique du Sud','RSA',2)
,(NEXTVAL('equipes_id_seq'),'Italie','ITA',2)
,(NEXTVAL('equipes_id_seq'),'Canada','CAN',2)
;