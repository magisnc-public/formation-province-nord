CREATE TABLE public.joueurs (
	id serial NOT NULL,
	id_equipe int8 NOT NULL,
	poste varchar NOT NULL,
	prenom varchar NOT NULL,
	nom varchar NOT NULL,
	nb_selections varchar NOT NULL,
	date_naissance date NOT NULL,
	CONSTRAINT joueurs_pk PRIMARY KEY (id),
	CONSTRAINT joueurs_un UNIQUE (prenom, nom),
	CONSTRAINT joueurs_fk FOREIGN KEY (id_equipe) REFERENCES equipes(id)
);