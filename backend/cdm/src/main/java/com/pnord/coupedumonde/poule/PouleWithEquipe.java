package com.pnord.coupedumonde.poule;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.List;

public class PouleWithEquipe {

    String nom;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    List<String> equipes;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<String> getEquipes() {
        return equipes;
    }

    public void setEquipes(List<String> equipes) {
        this.equipes = equipes;
    }
}
