package com.pnord.coupedumonde.joueurs;

import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller Rest
 * Permet d'exposer un ensemble d'operations sur la resource Joueurs, sous forme d'api REST, accessible via des urls HTTP
 * La donnée est sérializée au format JSON
 */
@RestController
@RequestMapping("/joueurs")
@Api(value = "Joueurs", tags = {"Joueurs"})
public class JoueurController {

    private JoueurRepository joueurRepository;

    public JoueurController(JoueurRepository joueurRepository) {
        this.joueurRepository = joueurRepository;
    }

    /**
     * Recupère la liste de tous les joueurs d'une equipe
     * Si id = 0, la liste de tous les joueurs est renvoyée
     */
    @GetMapping
    public ResponseEntity<List<Joueur>> getJoueursParEquipe(@RequestParam("equipe") long id) {

        List<Joueur> joueurs = (id == 0) ? joueurRepository.findAll() : joueurRepository.findByEquipeId(id);
        return ResponseEntity.ok(joueurs);
    }

    @PostMapping
    public ResponseEntity<Joueur> createJoueur(@Valid @RequestBody Joueur joueur) {

        Joueur newJoueur = joueurRepository.save(joueur);

        return ResponseEntity.ok(newJoueur);
    }

    /**
     * ex: PUT http://localhost:8888/api/equipes8 + objet equipe au format json
     * Mets à jour une equipe via son id
     * @return objet equipe mise a jour
     */
    @PutMapping("/{id}")
    public ResponseEntity<Joueur> updateJoueurById(@PathVariable("id") long id, @Valid @RequestBody Joueur joueur) {

        Joueur test = joueurRepository.save(joueur) ;

        return ResponseEntity.ok(test);
    }

    /**
     * ex: DELETE http://localhost:8888/api/equipes8 + objet equipe au format json
     * Supprime une équipe par son id
     * @param id long
     * @return void, Response HTTP 200
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteJoueureById(@PathVariable("id") long id) {

        joueurRepository.deleteById(id);

        return ResponseEntity.ok().build();
    }
}