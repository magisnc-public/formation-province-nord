package com.pnord.coupedumonde.poule;

import com.pnord.coupedumonde.equipe.Equipe;
import com.pnord.coupedumonde.equipe.EquipeRepository;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller Rest
 * Permet d'exposer un ensemble d'operations sur la resource Poules, sous forme d'api REST, accessible via des urls HTTP
 * La donnée est sérializée au format JSON
 */
@RestController
@RequestMapping("/poules")
@Api(value = "Poules", tags = { "Poules" })
@Transactional
public class PouleController {

    private PouleRepository pouleRepository;

    private EquipeRepository equipeRepository;


    public PouleController( EquipeRepository equipeRepository, PouleRepository pouleRepository) {
        this.pouleRepository = pouleRepository;
        this.equipeRepository = equipeRepository;
    }

    /**
     * ex: GET http://localhost:8888/api/poules
     *
     * Recupère toutes les équipes
     * @return une liste d'equipes au format json
     */
    @GetMapping
    public ResponseEntity<List<Poule>> getPoules() {

        List<Poule> poules = pouleRepository.findAll();

        return ResponseEntity.ok(poules);
    }

    /**
     * ex: GET http://localhost:8888/api/poules/8
     * Récupère une équipe par son id
     * @return une equipe
     */
    @GetMapping("/{id}")
    public ResponseEntity<Poule> fetchById(@PathVariable("id") long id) {

        Optional<Poule> optionalPoule = pouleRepository.findById(id);

        return optionalPoule.isPresent() ? ResponseEntity.ok(optionalPoule.get()) : ResponseEntity.notFound().build();
    }


    /**
     * ex: POST http://localhost:8888/api/equipes + objet equipe au format json
     * Ajoute une equipe
     * @return l'equipe nouvellement créée
     */
    @PostMapping
    public ResponseEntity<Poule> createPouleWithEquipes(@Valid @RequestBody PouleWithEquipe newPoule) {

        Optional<Poule> optPoule = pouleRepository.findByNom(newPoule.getNom());

        if (optPoule.isPresent()) {
            throw new IllegalArgumentException("Cette poule existe déjà");
        }

        List<Equipe> equipes = newPoule.equipes.stream().map(equipe -> equipeRepository.findByNom(equipe).get()).collect(Collectors.toList());

        Poule poule = new Poule();
        poule.setNom(newPoule.getNom());

        Poule nw = pouleRepository.save(poule);

        equipes.forEach(f -> {f.setPoule(nw); equipeRepository.save(f);});

        return ResponseEntity.ok(nw);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletebyId(@PathVariable("id") long id) {

       Optional<Poule> optPoule =  pouleRepository.findById(id);

       if (!optPoule.isPresent()) {
           throw new IllegalArgumentException("id non valide");
       }
       Poule poule = optPoule.get();

       poule.getEquipes().forEach(f-> {f.setPoule(null);equipeRepository.save(f);});

        pouleRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
