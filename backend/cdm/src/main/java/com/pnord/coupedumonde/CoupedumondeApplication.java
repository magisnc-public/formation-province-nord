package com.pnord.coupedumonde;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Classe de Bootstrap, celle qui permet de démarrer le serveur
 */
@EnableScheduling
@SpringBootApplication
public class CoupedumondeApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(CoupedumondeApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CoupedumondeApplication.class, args);
	}
}
