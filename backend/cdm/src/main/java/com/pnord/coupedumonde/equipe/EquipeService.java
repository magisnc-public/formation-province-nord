package com.pnord.coupedumonde.equipe;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Bean spring de type service
 * Permet d'encapsuler la logique métier, ici pas forcément utile, juste pour l'example
 */
@Service
public class EquipeService {

    private EquipeRepository equipeRepository;

    public EquipeService(EquipeRepository equipeRepository) {
        this.equipeRepository = equipeRepository;
    }

    public List<Equipe> getEquipes() {
        return this.equipeRepository.findAll();
    }

    public List<Equipe> fetchEquipesSansPoule() {
        return this.equipeRepository.findByPouleIsNull();
    }

    public Optional<Equipe> getEquipeById(long idEquipe) {
        return this.equipeRepository.findById(idEquipe);
    }

    public void deleteById(long id) {
        this.equipeRepository.deleteById(id);
    }

    public Equipe updateEquipe(Equipe equipe) {
        Optional<Equipe> optionalEquipe =  this.equipeRepository.findById(equipe.getId());

        if (!optionalEquipe.isPresent()) {
            throw new IllegalArgumentException("id non valide.");
        }
        Equipe equipeDb = optionalEquipe.get();
        equipeDb.setCode(equipe.getCode());
        equipeDb.setNom(equipe.getNom());

       return  this.equipeRepository.save(equipeDb);
    }

    public Equipe addEquipe(Equipe equipe) {
        return  this.equipeRepository.save(equipe);
    }
}
