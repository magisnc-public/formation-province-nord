package com.pnord.coupedumonde.equipe;

/**
 * Projection basée sur une interface java
 * Objet utile pour controler les champs que l'on souhaite restiture cote frontend
 */
public interface CodeOnly {

    String getCode();
}
