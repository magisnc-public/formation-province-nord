package com.pnord.coupedumonde.poule;

import com.pnord.coupedumonde.equipe.Equipe;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Entité JPA
 * Objet qui sert à mapper une table et les colonnes de la base de données, ici table joueurs
 */
@Entity
@Table(name="poules")
public class Poule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 3, max = 20)
    private String nom;

    @OneToMany(mappedBy="poule", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    List<Equipe> equipes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(List<Equipe> equipes) {
        this.equipes = equipes;
    }

    @Override
    public String toString() {
        return "Poule{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }
}
