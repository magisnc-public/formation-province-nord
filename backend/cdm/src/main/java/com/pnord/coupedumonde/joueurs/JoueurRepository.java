package com.pnord.coupedumonde.joueurs;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Interface Spring de Type Repository
 * Propose un ensemble de méthodes permettant d'accéder aux données Joueurs
 * Couche d'abstraction facilitant la communication avec la base de données
 */
public interface JoueurRepository extends JpaRepository<Joueur, Long> {


    List<Joueur> findByEquipeId(long id);

    List<NameJoueurOnly> findAllBy();
}
