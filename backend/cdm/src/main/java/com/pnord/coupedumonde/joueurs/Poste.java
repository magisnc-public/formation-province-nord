package com.pnord.coupedumonde.joueurs;

/**
 * Enumeration java sur les différents type de poste
 */
public enum Poste {

    HOOKER, BACK_ROW, WING,FLY_HALF, SCRUM_HALF, CENTRE, LOCK, LOOSE_FORWARD, PROP, FULLBACK;
}
