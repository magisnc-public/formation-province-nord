package com.pnord.coupedumonde.poule;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Interface Spring de Type Repository
 * Propose un ensemble de méthodes permettant d'accéder aux données Equipes
 * Couche d'abstraction facilitant la communication avec la base de données en proposant des méthodes CRUD
 */
public interface PouleRepository extends JpaRepository<Poule, Long> {

    Optional<Poule> findByNom(String nom);

}
