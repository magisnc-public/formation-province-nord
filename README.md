# Backend

## Spring

## Démarrer le serveur
* java -jar coupedumonde-0.0.1-SNAPSHOT.jar --spring.config.location=application.properties

#### Starters
* https://start.spring.io

#### Spring
* https://spring.io

#### Documentation spring-boot
* https://docs.spring.io/spring-boot/docs/current/reference/pdf/spring-boot-reference.pdf
* Propriétes spring-boot page 359

### Documentation Spring-framework
* https://docs.spring.io/spring/docs/current/spring-framework-reference/overview.html#overview

### Documentation Spring-data-jpa
* https://docs.spring.io/spring-data/jpa/docs/2.2.0.RELEASE/reference/html/#reference

## Editeurs

#### Jetbrains
* https://www.jetbrains.com

#### Flyway
* https://flywaydb.org/getstarted/how

## Maven repository
* https://mvnrepository.com/

# Frontend

