import { NgModule } from '@angular/core';
import { PoulesComponent } from './poules.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: PoulesComponent }
];

@NgModule({
  imports: [
      RouterModule.forChild(routes)
  ],
  declarations: [PoulesComponent],
  exports: [PoulesComponent],
  providers: []
})
export class PoulesModule { }
