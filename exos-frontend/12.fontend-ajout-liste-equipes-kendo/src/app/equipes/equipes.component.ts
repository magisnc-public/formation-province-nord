import { Component, OnInit } from '@angular/core';
import { EquipeService } from './equipe.service';
import {
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { SortDescriptor, process, State } from '@progress/kendo-data-query';

@Component({
  selector: 'app-equipes',
  templateUrl: './equipes.component.html',
  styleUrls: ['./equipes.component.scss']
})
export class EquipesComponent implements OnInit {
  public multiple = false;
  public allowUnsort = true;

  items = [];

  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{ field: 'Nom', operator: 'contains', value: '' }]
    },
    sort: [{
      field: 'nom',
      dir: 'asc'
    }]
  };

  public gridData: GridDataResult;

  constructor(private equipeSvc: EquipeService) {}

  ngOnInit() {
    this.equipeSvc.fetchEquipes().subscribe(res => this.test(res));
  }

  public test(res) {
    this.items = res;
    this.gridData = process(res, this.state);
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gridData = process(this.items, this.state);
  }
}
