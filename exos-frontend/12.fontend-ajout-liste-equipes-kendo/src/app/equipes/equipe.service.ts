import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {
  constructor(private httpClient: HttpClient) {}

  public fetchEquipes(): any {
    return this.httpClient.get('/api/equipes');
  }

  public fetchEquipe(id: number): any {
    return this.httpClient.get('/api/equipes/' + id);
  }
}
