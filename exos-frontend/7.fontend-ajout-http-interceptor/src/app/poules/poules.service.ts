import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class PouleService {
  constructor(private httpClient: HttpClient) {}

  public fetchPoules(): any {
    return this.httpClient.get("/api/poules");
  }
}
