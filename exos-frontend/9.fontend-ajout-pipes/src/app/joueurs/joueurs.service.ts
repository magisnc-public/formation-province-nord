import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class JoueursService {
  constructor(private httpClient: HttpClient) {}

  public fetchJoueurs(): Observable<any> {
    return this.httpClient.get("/api/joueurs?equipe=0");
  }
}
