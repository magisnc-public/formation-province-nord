import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-poule',
  templateUrl: './form-add.component.html',
  styleUrls: ['./form-add.component.scss']
})
export class AddPouleComponent implements OnInit {

  @Input() equipesSansPoules = [];

  @Output() submitEvent = new EventEmitter();

  @Output() cancelEvent = new EventEmitter();

  poulesForm: FormGroup;

  constructor() {}

  ngOnInit() {
    this.poulesForm = new FormGroup({
      nom: new FormControl('', Validators.required),
      equipes: new FormControl('', Validators.required)
    });
  }

  cancel() {
    this.cancelEvent.emit(null);
  }

  submitPoule() {
    this.submitEvent.emit(this.poulesForm.value);
  }

  sort(data: any[]): any[] {
    return data.sort((a, b) => (a > b ? 1 : b > a ? -1 : 0));
  }
}
