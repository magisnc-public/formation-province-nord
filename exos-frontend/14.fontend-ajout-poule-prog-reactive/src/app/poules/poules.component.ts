import { Component, OnInit } from '@angular/core';
import { PouleService } from './poules.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { forkJoin, concat } from 'rxjs';
import { skip } from 'rxjs/operators';

@Component({
  selector: 'app-poules',
  templateUrl: './poules.component.html',
  styleUrls: ['./poules.component.scss']
})
export class PoulesComponent implements OnInit {
  poules: any = [];
  equipesSansPoules: any = [];

  displayForm = false;

  constructor(private poulesSvc: PouleService) {}

  ngOnInit() {
    const poules$ = this.poulesSvc.fetchPoules();
    const equipesSansPoules$ = this.poulesSvc.fetchEquipeSansPoules();

    const answer$ = forkJoin(poules$, equipesSansPoules$);
    answer$.subscribe(([res1, res2]) => {
      this.poules = res1;
      this.equipesSansPoules = res2;
    });
  }

  showForm() {
    this.displayForm = true;
  }

  submitPoule(data) {
    const add$ = this.poulesSvc.addPoule(data);
    const equipesSansPoules$ = this.poulesSvc.fetchEquipeSansPoules();
    const poule$ = this.poulesSvc.fetchPoules();

    // 2 requetes peuvent etre faites en //
    const res$ = forkJoin(equipesSansPoules$, poule$);

    const arr = [add$, res$];

    // seules les 2 dernières réponses HTTP m'interessent (poules && equipesSansPoules)
    // concat me permet de conserver l'ordre des observables, et donc d"executer la 1ère request POST et ensuite
    // d'executer les 2 autres en //
    concat(...arr)
      .pipe(skip(1))
      .subscribe(res => this.updateView(res[0], res[1]));
  }

  deletePoule(id) {
    const equipesSansPoules$ = this.poulesSvc.fetchEquipeSansPoules();
    const deletePoule$ = this.poulesSvc.deletePoule(id);
    const poule$ = this.poulesSvc.fetchPoules();

    // 2 requetes peuvent etre faites en //
    const res$ = forkJoin(equipesSansPoules$, poule$);
    const arr = [deletePoule$, res$];

    // concat me permet de conserver l'ordre des observables, et donc d"executer la 1ère request DELETE et ensuite
    // d'executer les 2 autres en //
    concat(...arr)
      .pipe(skip(1))
      .subscribe(res => this.updateView(res[0], res[1]));
  }

  /**
   * Mise à jour de la vue avec les nouvelles valeurs des poules et des equipes sans poules
   * @param res : equipesSansPoule
   * @param res2 : liste des poules
   */
  updateView(res, res2) {
    this.equipesSansPoules = res;
    this.poules = res2;
    this.displayForm = false;
  }

  cancel() {
    this.displayForm = false;
  }
}

export interface Poule {
  nom: string;
  equipes: any[];
}
