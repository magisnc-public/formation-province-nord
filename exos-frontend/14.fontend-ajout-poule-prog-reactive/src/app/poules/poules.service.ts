import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PouleService {
  constructor(private httpClient: HttpClient) {}

  public fetchPoules(): Observable<any> {
    return this.httpClient.get('/api/poules');
  }

  public fetchEquipeSansPoules(): Observable<any> {
    return this.httpClient.get('/api/equipes/sanspoules');
  }

  public addPoule(data): Observable<any> {
    return this.httpClient.post('/api/poules', data);
  }

  public deletePoule(id): Observable<any> {
    return this.httpClient.delete('/api/poules/' + id);
  }
}
