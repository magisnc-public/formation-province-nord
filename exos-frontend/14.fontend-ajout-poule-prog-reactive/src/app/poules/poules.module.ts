import { NgModule } from '@angular/core';
import { PoulesComponent } from './poules.component';
import { Routes, RouterModule } from '@angular/router';
import { PouleComponent } from './poule/poule.component';
import { CommonModule } from '@angular/common';
import { PouleService } from './poules.service';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { ReactiveFormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { AddPouleComponent } from './form-add/form-add.component';

const routes: Routes = [
  { path: '', component: PoulesComponent }
];

@NgModule({
  imports: [
      CommonModule,
      ReactiveFormsModule,
      RouterModule.forChild(routes),
      ButtonsModule,
      InputsModule,
      DropDownsModule
  ],
  declarations: [PoulesComponent, PouleComponent, AddPouleComponent],
  exports: [PoulesComponent, PouleComponent, AddPouleComponent],
  providers: [PouleService]
})
export class PoulesModule { }
