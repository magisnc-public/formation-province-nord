import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class EquipeService {
  constructor(private httpClient: HttpClient) {}

  public fetchEquipes(): any {
    return this.httpClient.get("/api/equipes");
  }

  public fetchEquipe(id: number): any {
    return this.httpClient.get("/api/equipes/" + id);
  }

  public addEquipe(data: any): any {
    return this.httpClient.post("/api/equipes/", data);
  }

  public updateEquipe(data: any): any {
    return this.httpClient.put("/api/equipes/" + data.id, data);
  }

  public deleteEquipe(id: number): any {
    return this.httpClient.delete("/api/equipes/" + id);
  }
}
