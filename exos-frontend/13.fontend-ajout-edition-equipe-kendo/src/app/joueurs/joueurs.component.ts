import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { JoueursService } from './joueurs.service';

@Component({
  selector: 'app-joueurs',
  templateUrl: './joueurs.component.html',
  styleUrls: ['./joueurs.component.scss']
})
export class JoueursComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource = new MatTableDataSource<any>();

  displayedColumns: string[] = [ 'nom', 'prenom', 'poste', 'dateNaissance', 'nbSelections'];

  constructor(private joueursSvc: JoueursService) {}

  ngOnInit() {
    this.joueursSvc
      .fetchJoueurs()
      .subscribe(res => this.dataSource.data = res);
    this.dataSource.paginator = this.paginator;
  }
}
