import { NgModule } from '@angular/core';
import { JoueursComponent } from './joueurs.component';
import { Routes, RouterModule } from '@angular/router';
import { JoueursService } from './joueurs.service';
import { MatTableModule, MatPaginatorModule } from '@angular/material';
import { MomentModule } from 'ngx-moment';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: '', component: JoueursComponent }
];

@NgModule({
  imports: [
      SharedModule,
      MatTableModule,
      MatPaginatorModule,
      MomentModule,
      RouterModule.forChild(routes)
  ],
  declarations: [JoueursComponent],
  exports: [JoueursComponent],
  providers: [ JoueursService],
})
export class JoueursModule { }
