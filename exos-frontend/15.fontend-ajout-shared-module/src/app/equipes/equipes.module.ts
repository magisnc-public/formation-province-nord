import { NgModule } from '@angular/core';
import { EquipesComponent } from './equipes.component';
import { Routes, RouterModule } from '@angular/router';
import { EquipeDetailComponent } from './equipe-detail/equipe-detail.component';
import { EquipeService } from './equipe.service';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: '', component: EquipesComponent },
  { path: ':id', component: EquipeDetailComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EquipesComponent, EquipeDetailComponent],
  exports: [EquipesComponent, EquipeDetailComponent],
  providers: [EquipeService]
})
export class EquipesModule {}
