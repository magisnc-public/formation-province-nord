import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Poule } from '../poules.component';

@Component({
  selector: 'app-poule',
  templateUrl: './poule.component.html',
  styleUrls: ['./poule.component.scss']
})
export class PouleComponent {
  @Input() poule: Poule;
  @Output() deleteEvent = new EventEmitter();

  showForm = false;

  /** Méthode qui permet de trier les équipes */
  sort(equipes: any[]): any[] {
    return equipes.sort((a, b) => (a.nom > b.nom ? 1 : b.nom > a.nom ? -1 : 0));
  }

  deletePoule(id) {
    this.deleteEvent.emit(id);
  }
}
