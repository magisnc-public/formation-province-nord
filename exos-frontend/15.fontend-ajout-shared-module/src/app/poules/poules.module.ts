import { NgModule } from '@angular/core';
import { PoulesComponent } from './poules.component';
import { Routes, RouterModule } from '@angular/router';
import { PouleComponent } from './poule/poule.component';
import { PouleService } from './poules.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AddPouleComponent } from './form-add/form-add.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: '', component: PoulesComponent }
];

@NgModule({
  imports: [
      SharedModule,
      ReactiveFormsModule,
      RouterModule.forChild(routes)
  ],
  declarations: [PoulesComponent, PouleComponent, AddPouleComponent],
  exports: [PoulesComponent, PouleComponent, AddPouleComponent],
  providers: [PouleService]
})
export class PoulesModule { }
