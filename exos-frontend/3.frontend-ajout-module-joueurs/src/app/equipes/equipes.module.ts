import { NgModule } from '@angular/core';
import { EquipesComponent } from './equipes.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: EquipesComponent }
];

@NgModule({
  imports: [
      RouterModule.forChild(routes)
  ],
  declarations: [EquipesComponent],
  exports: [EquipesComponent],
  providers: []
})
export class EquipesModule { }
