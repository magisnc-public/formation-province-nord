import { NgModule } from '@angular/core';
import { EquipesComponent } from './equipes.component';
import { Routes, RouterModule } from '@angular/router';
import { EquipeDetailComponent } from './equipe-detail/equipe-detail.component';
import { EquipeService } from './equipe.service';
import { CommonModule } from '@angular/common';
import { GridModule, PDFModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { InputsModule } from '@progress/kendo-angular-inputs';

const routes: Routes = [
  { path: '', component: EquipesComponent },
  { path: ':id', component: EquipeDetailComponent }
];

@NgModule({
  imports: [
      CommonModule,
     GridModule,
      InputsModule,
      PDFModule,
     DropDownsModule,
      DateInputsModule,
      RouterModule.forChild(routes)
  ],
  declarations: [EquipesComponent, EquipeDetailComponent],
  exports: [EquipesComponent, EquipeDetailComponent],
  providers: [EquipeService]
})
export class EquipesModule { }
