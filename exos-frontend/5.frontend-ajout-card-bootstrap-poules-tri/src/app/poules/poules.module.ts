import { NgModule } from '@angular/core';
import { PoulesComponent } from './poules.component';
import { Routes, RouterModule } from '@angular/router';
import { PouleComponent } from './poule/poule.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { PouleService } from './poules.service';

const routes: Routes = [
  { path: '', component: PoulesComponent }
];

@NgModule({
  imports: [
      HttpClientModule,
      CommonModule,
      RouterModule.forChild(routes)
  ],
  declarations: [PoulesComponent, PouleComponent],
  exports: [PoulesComponent, PouleComponent],
  providers: [PouleService]
})
export class PoulesModule { }
