import { NgModule } from '@angular/core';
import { JoueursComponent } from './joueurs.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: JoueursComponent }
];

@NgModule({
  imports: [
      RouterModule.forChild(routes)
  ],
  declarations: [JoueursComponent],
  exports: [JoueursComponent],
  providers: []
})
export class JoueursModule { }
