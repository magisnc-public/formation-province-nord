import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class TokenGuard implements CanActivate {

    // possibilité d'injecter des services, etc,...
    constructor() {}

    canActivate() {
       if (!localStorage.getItem('Authorization')) {
            console.warn('Token absent. Redirection vers la page de login');
            return false;
       }
       return true;
    }
}