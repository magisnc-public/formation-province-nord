import { Component, OnInit } from '@angular/core';
import { EquipeService } from './equipe.service';
import {
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { concat } from 'rxjs';
import { toArray } from 'rxjs/operators';

@Component({
  selector: 'app-equipes',
  templateUrl: './equipes.component.html',
  styleUrls: ['./equipes.component.scss']
})
export class EquipesComponent implements OnInit {
  public formGroup: FormGroup;

  private editedRowIndex: number;

  items = [];

  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{ field: 'Nom', operator: 'contains', value: '' }]
    },
    sort: [
      {
        field: 'nom',
        dir: 'asc'
      }
    ]
  };

  public gridData: GridDataResult;

  constructor(private equipeSvc: EquipeService) {}

  ngOnInit() {
    this.equipeSvc.fetchEquipes().subscribe(res => this.updateGrid(res));
  }

  public updateGrid(res) {
    this.items = res;
    this.gridData = process(res, this.state);
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gridData = process(this.items, this.state);
  }

  /**
   * Gestion de l'éditeur avec formulaire Angular
   * @param
   */
  public addHandler({ sender }) {
    this.formGroup = new FormGroup({
      code: new FormControl('', Validators.required),
      nom: new FormControl('', Validators.required)
    });

    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    // define all editable fields validators and default values
    const group = new FormGroup({
      id: new FormControl(dataItem.id, Validators.required),
      code: new FormControl(dataItem.code, Validators.required),
      nom: new FormControl(dataItem.nom, Validators.required)
    });

    // put the row in edit mode, with the `FormGroup` build above
    sender.editRow(rowIndex, group);
  }

  /**
   * Ferme l'éditeur
   * @param param0
   */
  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  /**
   * Persiste une nouvelle équipe
   * @param param0
   */
  public saveHandler({ sender, rowIndex, formGroup, isNew }) {
    const equipe: any = formGroup.value;
    const fetch$ = this.equipeSvc.fetchEquipes();

    if (isNew) {
      const add$ = this.equipeSvc.addEquipe(equipe);
      const arr = [add$, fetch$];
      concat(...arr)
        .pipe(toArray())
        .subscribe(res => this.updateGrid(res[1]));
    } else {
      const update$ = this.equipeSvc.updateEquipe(equipe);
      const arr = [update$, fetch$];
      concat(...arr)
        //les valeurs sont mises dans un tableau
        .pipe(toArray())
        .subscribe(res => this.updateGrid(res[1]));
    }
    sender.closeRow(rowIndex);
  }

  /**
   *  Supprime une équipe
   * @param param
   */
  public removeHandler({ dataItem }) {
    const remove$ = this.equipeSvc.deleteEquipe(dataItem.id);
    const fetch$ = this.equipeSvc.fetchEquipes();
    const arr = [remove$, fetch$];
    concat(...arr)
      .pipe(toArray())
      .subscribe(res => this.updateGrid(res[1]));
  }

  /**
   *
   * @param grid Ferme l'editeur
   * @param rowIndex
   */
  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }
}
