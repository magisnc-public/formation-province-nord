import { Component, OnInit } from '@angular/core';
import { EquipeService } from '../equipe.service';
import { ActivatedRoute } from '@angular/router';
import { State, process } from '@progress/kendo-data-query';
import {
  GridDataResult,
  DataStateChangeEvent,
} from '@progress/kendo-angular-grid';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'app-equipe-detail',
  templateUrl: './equipe-detail.component.html',
  styleUrls: ['./equipe-detail.component.scss']
})
export class EquipeDetailComponent implements OnInit {
  equipe: any;
  totaSelectionsTeam = 0;
  minSelectionPlayer = '';
  maxSelectionPlayer = '';

  public state: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{ field: 'nom', operator: 'contains', value: '' }]
    },
    sort: [
      {
        field: 'nom',
        dir: 'asc'
      }
    ]
  };

  public gridData: GridDataResult;

  constructor(
    private route: ActivatedRoute,
    private equipeSvc: EquipeService
  ) {}

  ngOnInit() {
   const params$ =  this.route.params;
   // evite de faire un subscribe dans un subscribe
   const result$ = params$.pipe(concatMap((params) =>  this.equipeSvc.fetchEquipe(params.id)));
   result$.subscribe((res) => this.build(res));
  }

  build(res) {
    this.equipe = res;
    this.gridData = process(this.equipe.joueurs, this.state);
    this.makeSomeCalculs();
  }

  makeSomeCalculs() {
    if (this.equipe.joueurs.length === 0) {
      return;
    }

    this.totaSelectionsTeam = this.equipe.joueurs
      .map(x => x.nbSelections)
      .reduce((acc, curr) => acc + curr, 0);
    const selectionMin = Math.min(
      ...this.equipe.joueurs.map(x => x.nbSelections)
    );
    this.minSelectionPlayer = this.equipe.joueurs.find(
      elt => elt.nbSelections === selectionMin
    ).nom;
    const selectionMax = Math.max(
      ...this.equipe.joueurs.map(x => x.nbSelections)
    );
    this.maxSelectionPlayer = this.equipe.joueurs.find(
      elt => elt.nbSelections === selectionMax
    ).nom;
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gridData = process(this.equipe.joueurs, this.state);
  }
}
