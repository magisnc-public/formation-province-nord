import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { NgModule } from '@angular/core';
import { PDFModule, GridModule } from '@progress/kendo-angular-grid';


@NgModule({
    imports: SharedModule.MODULE_LIST,
    exports: SharedModule.MODULE_LIST,
    providers: []
})
export class SharedModule {

    static readonly MODULE_LIST = [
        CommonModule,
        RouterModule,
        ButtonsModule,
        InputsModule,
        DropDownsModule,
        GridModule,
        PDFModule
    ];
}