import { HttpInterceptor, HttpRequest, HttpResponse, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class RugbyInterceptor implements HttpInterceptor {

    private tokenName = 'Authorization';

    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const authReq = req.clone({
            headers: req.headers.set('Authorization', 'Bearer ' +
                localStorage.getItem(this.tokenName))
        });

        return next.handle(authReq);
    }
}
